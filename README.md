# BLDC Motor with Resin Cast Iron

## UPDATE

Made one big old mistake here, which you shouldn't follow, which was to omit the back iron on the magnet ring, so there's no flux return path. This is bad. Do not do what I did. Sometime I will try this again, and do it properly.

The CAD, however, is [here](/cad/Fab_BLDC.f3d)

## Motivation

I want to build a brushless dc motor. These are incredibly common in Mechatronics, and I would say that in order to make *really neat shit* we have to be able to develop not only *assemblies* of components, but *components* themselves. I.E - our own sensors, motors, gearboxes, bearings, etc...

```ASCII
¯\_(ツ)_/¯ 
```

## Design Begins...

OK, so we have a BLDC here:

![inrunner animation](https://gitlab.cba.mit.edu/jakeread/fabbldc/raw/master/images/inrunner-animation.gif)

Critically, there are *pole pairs* (2 of those in this diagram) on the rotor (the thing that spins) and *phases* (6 of those in this diagram). We can also unroll this -> 

![unroller](https://gitlab.cba.mit.edu/jakeread/fabbldc/raw/master/images/motor-unroller.jpg)

So we see that for every *pair* of poles we have (and we must have pairs) we have three phases. In the motor nomenclature, people tend to talk about how many *pole pairs* a motor has, as this is the defining feature. A BLDC can have one pole-pair (one magnet spinning around three phases) or > 100 pole pairs! For example, Stepper Motors have 100 or 200 pole pairs<sup>1</sup>.

Let's go through a brief list of our design variables:

**# of Pole Pairs**
 - Number of poles, as discussed above
 - Trend is towards more torque w/ more pole pairs: we are 'never far away from a pole'. We *kind of* trade this for speed. 
 - Typical ranges are 4 (for ~ 20krpm motors) towards 21->30 (for ~1krpm, high torque). 

**R_gap**
 - Radius of the 'air gap' the gap between the phases and the magnets. This is directly related to the moment arm that the motor is 'pulling' things around with, so Torque tends to go to the 2nd with this variable. See [Biomemetic Robotics](http://biomimetics.mit.edu/research/optimal-actuator-design) for some neat discussion on this. Particularely, see [this paper](http://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=7827048) and Figure 6.
 - Critically, while motor torque scales with the 2nd w/r/t R_gap, motor *inertia* goes to the 3rd. So, in systems we want to control quickly, inertia is bad, so this is bad. HOWEVER - it may allow us to choose a lower gear ratio, thus decreasing the speed of our motor (for the same speed of the output). See how complicated this can get?

**# Windings**
 - Each phase has a coil wound around it some # of times. This, along with *many* other factors, affects how much power you can pump through the motor. Essentially, the more windings, the more magnetic field you get (per unit of current) - see [this](http://hyperphysics.phy-astr.gsu.edu/hbase/magnetic/solenoid.html) set of drawings & equations<sup>2</sup>. However, as I increase the # of windings I'm decreasing the size of the wire, so I'm limiting the amperage I'll be able to push through the motor without cooking it. See! Design, exciting! See the Biomemetics motors [here (video)](https://youtu.be/p8ozov_xymM?t=1m15s) to get a sense for how dense a *good motor's* windings are. Hint: dense as heck.

**Motor Length**
 - Or, thickness. How deep? More deep = more power? Yes, but linear.

## Modelling Begins

OK OK! I feel OK about all of these things. I'm going to dive into it with Fusion. 

![roughdesign](https://gitlab.cba.mit.edu/jakeread/fabbldc/raw/master/images/motor-design-rough.jpg)

My key constraint here is that I have to *buy* magnets. Heck. I found [these](https://supermagnetman.com/collections/neo-arcs/products/m5005?variant=23728828419) around the size I'd like. Big OD, thin in both directions, so lightweight. Noice. The design above is spec'd for that.

Now I'm going to draw some get-down around the motor: bearings, housings. I want to mill these bearings also, as another experiment. 

![roughdesign](https://gitlab.cba.mit.edu/jakeread/fabbldc/raw/master/images/support-halfsectional.jpg)

OK, I filled this in with *the details* 

![fusion-section](https://gitlab.cba.mit.edu/jakeread/fabbldc/raw/master/images/fusion-section.png)

![fusion-axo](https://gitlab.cba.mit.edu/jakeread/fabbldc/raw/master/images/fusion-axo.png)

Now I'm feeling OK about this so I'm going to drop it into Rhino to prepare the milling files. 

## Cam

Nice - look at this adaptive path I get to use! 

![cam-adaptive](https://gitlab.cba.mit.edu/jakeread/fabbldc/raw/master/images/cam-adaptive.jpg)

This side was more of a doozy - had to set up sketch profiles to trace the inside of my bearing race properly.

![cam-doozy](https://gitlab.cba.mit.edu/jakeread/fabbldc/raw/master/images/cam-doozy.jpg)

## Magnets Arrive

Update: I got these in the mail. I printed some parts. They fit!

![magnets-fit](https://gitlab.cba.mit.edu/jakeread/fabbldc/raw/master/images/magnets-fit.jpg)

## The Milling

OK I did the milling... gif of this adaptive toolpath for proof:

![doing-the-milling](https://gitlab.cba.mit.edu/jakeread/fabbldc/raw/master/images/doing-the-milling.gif)

Noice. Here's the part:

![milled-positive](https://gitlab.cba.mit.edu/jakeread/fabbldc/raw/master/images/milled-positive.jpg)

## Casting

I'm using [High Temperature Laminating Epoxy](https://www.smooth-on.com/products/epoxamite-ht/) and [5 Micron Iron Dust](https://www.mcmaster.com/#1402n25/=1akie10) from McMaster. 

The cast iron is probably going to be *very* brittle. I'm going to cast it in a semi-soft mould - the Red Urethane we have at the lab. I'll machine a positive out of HDPE or Delrin to start then cast the negative. I'll put that negative on a rigid backing (to avoid some dimensional changes under vacuum) and then use a release & a filter fabric (down to 5 microns - the iron size - this is *small*) to push the epoxy out while keeping the iron in. Noice. I should end up with a dense core, maximizing permeability, which maximizes the field strength, etc.

![casting diagram](https://gitlab.cba.mit.edu/jakeread/fabbldc/raw/master/images/casting-diagram.jpg)

First I cast some Smooth-On Mold Max 60 into the positive, to get a negative. I degassed it for a while (MM60 has a pot life of 40 minutes, so no rush! nice!), then I put it in the mold. After spraying some release on the mold.

![ok-mold-one](https://gitlab.cba.mit.edu/jakeread/fabbldc/raw/master/images/ok-mold-one.jpg)

OK - ttyl composites week, I have 24 hours until this is cured. Then I do the resin / iron casting, then I do the winding, and then I do the motor turning? TBD!

## Winding

I cast the motor core, using a roughly 3:1 Iron : Resin ratio - I used high temp resin because motors are basically heat limited. Future work: cast water cooling channels into motor core.

![ok-mold-one](https://gitlab.cba.mit.edu/jakeread/fabbldc/raw/master/images/core-cast.jpg)

Winding turns out to be fairly straightforward. The key is to think about it as a rolled-up linear thing, then you're just winding each phase one at a time in a line, maintaining that orientation as you wrap around.

![ok-mold-one](https://gitlab.cba.mit.edu/jakeread/fabbldc/raw/master/images/winding-one.jpg)

![ok-mold-one](https://gitlab.cba.mit.edu/jakeread/fabbldc/raw/master/images/winding-two.jpg)

![ok-mold-one](https://gitlab.cba.mit.edu/jakeread/fabbldc/raw/master/images/winding-three.jpg)

## Assembly

Brought this online

![ok-mold-one](https://gitlab.cba.mit.edu/jakeread/fabbldc/raw/master/images/motor-complete.jpg)

## Wakeup

I tried turning the motor around with a cheap BLDC controller (i.e. hobbyking ESC) but this was unsatisfactory. In future work, I'm developing a [BLDC Driver](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver) where I can exert more control over the power I'm driving to the coils, as well as measuring phase voltages and currents. I'll use that to spin it around.

So, it nearly spins - I was able to (later on) hook it up to the BLDC Controller mentioned above. Here you can at least hear it trying to spin ;) TBD: video of it really turning.

[Video Here](https://gitlab.cba.mit.edu/jakeread/fabbldc/raw/master/video/nearly-turning.m4v)

# Conclusion

This feels really good - and I *really* like the idea of being able to make motors of my own design / shape / size etc. Full out design integration is a [winning idea](https://gitlab.cba.mit.edu/jakeread/spacecadet/tree/master) as far as I can tell. 

Next big step would be to measure the magnetic permeability of the core - that's the key step here. Then I can build realistic motor models and develop a *really good spreadsheet* to calculate (roughly) motor constants while I design. 

# Footnotes
1. This is a reduction - there is other fanciness to their design that makes them not *strictly* the three-phase BLDC we are looking at here, but they do have ~ 100 pole pairs. 
2. Please keep in mind I'm not trying to build a *real* model of the motor / expected torque at this point - just trying to get a sense for the relevant design parameters. 