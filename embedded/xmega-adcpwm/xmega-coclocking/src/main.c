/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# "Insert system clock initialization code here" comment
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
#include <asf.h>
#include "adc.h"

// LEDS PD3 and PD3
// Button PD5

int16_t adcresult;
int16_t offset;

int main (void)
{
	
	OSC.CTRL = OSC_RC32MEN_bm; // enable 32MHz clock
	while (!(OSC.STATUS & OSC_RC32MRDY_bm)); // wait for clock to be ready
	CCP = CCP_IOREG_gc; // enable protected register change
	CLK.CTRL = CLK_SCLKSEL_RC32M_gc; // switch to 32MHz clock
			
	PORTD.DIRSET = PIN3_bm | PIN4_bm; // set output (leds)
	PORTD.DIRCLR = PIN5_bm; // set input (button)
	
	PORTC.DIRSET = PIN1_bm; // new data out
	PORTC.OUTSET = PIN1_bm;
	
	PORTC.PIN1CTRL = PORT_OPC_PULLDOWN_gc;
	
	//PORTA.DIRCLR = PIN1_bm;
	
	ADCA.CALL = SP_ReadCalibrationByte(PROD_SIGNATURES_START + ADCACAL0_offset);
	ADCA.CALH = SP_ReadCalibrationByte(PROD_SIGNATURES_START + ADCACAL1_offset);
	ADC_ConvMode_and_Resolution_Config(&ADCA, 1, ADC_RESOLUTION_8BIT_gc);
	ADC_Reference_Config(&ADCA, ADC_REFSEL_INTVCC_gc);
	ADC_Prescaler_Config(&ADCA, ADC_PRESCALER_DIV64_gc);
	
	ADC_Ch_InputMode_and_Gain_Config(&ADCA.CH0, ADC_CH_INPUTMODE_SINGLEENDED_gc, ADC_CH_GAIN_1X_gc);
	
	ADC_Ch_InputMux_Config(&ADCA.CH0, ADC_CH_MUXPOS_PIN1_gc, ADC_CH_MUXNEG_INTGND_MODE4_gc);
	
	//ADCA.CH0.INTCTRL = ADC_CH_INTMODE_COMPLETE_gc | ADC_CH_INTLVL_HI_gc;
	
	ADC_Enable(&ADCA);
	//delay_us(100);
	ADC_Wait_8MHz(&ADCA);
	
	adcresult = 1000;
	//offset = ADC_Offset_Get_Signed(&ADCA, &(ADCA.CH0), 100);
		
	PMIC.CTRL = PMIC_HILVLEN_bm;
	sei();
	
	uint16_t halfperiod = 385;
	
	while(1){
		ADC_Ch_Conversion_Start(&ADCA.CH0);
		//while(!(ADCA.CH0.INTFLAGS & ADC_CH_CHIF_bm)){};
		adcresult = ADCA.CH0.RES * 6;
		ADCA.CH0.INTFLAGS = ADC_CH_CHIF_bm;
		
		PORTC.DIRSET = PIN1_bm;
		delay_us(min((halfperiod + adcresult),halfperiod*2));
		PORTC.DIRCLR = PIN1_bm;
		delay_us(max((halfperiod - adcresult),2));
	}
}

ISR(ADCA_CH0_vect)
{
	adcresult = ADCA.CH0.RES; // ADC_ResultCh_GetWord(&ADCA.CH0);
	ADCA.CH0.INTFLAGS = ADC_CH_CHIF_bm;
}
